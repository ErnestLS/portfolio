//
//  ChurchLocation.swift
//  OrthodoxCalendar-HD
//
//   Ernest L.S. on 01.01.16.
//  Copyright © 2016 ErLS & Y. All rights reserved.
//

import Foundation
import MapKit


final class ChurchAnnotation: NSObject, MKAnnotation  {
    var churchId: String
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var imageStr: String?
    
    override init() {
        self.churchId = ""
        self.coordinate = CLLocationCoordinate2D()
        self.title = ""
        self.subtitle = ""
        self.imageStr = ""
    }
    
    init(churchId: String, latitude: Double, longitude: Double, note:String, imageStr: String) {
        self.churchId = churchId
        self.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
        self.title = note
        self.imageStr = imageStr
    }
}
