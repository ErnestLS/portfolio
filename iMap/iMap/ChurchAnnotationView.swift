//
//  ChurchAnnotationView.swift
//  OrthodoxCalendar-HD
//
//   Ernest L.S. on 13.01.16.
//  Copyright © 2016 ErLS & Y. All rights reserved.
//

import MapKit

class ChurchAnnotationView: MKAnnotationView {
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
    }

//    override init(frame: CGRect) {
//        super.init(frame: frame)
//    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard let hitView = super.hitTest(point, with: event)
            else { return nil }
        
        self.superview!.bringSubviewToFront(self)
        return hitView
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let rect:CGRect = self.bounds
        var isInside:Bool = rect.contains(point)
        if(!isInside)
        {
            for view: UIView in self.subviews
            {
                isInside = view.frame.contains(point)
                if(isInside) { break }
            }
        }
        return isInside
    }
    
}
