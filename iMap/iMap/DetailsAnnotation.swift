//
//  DetailsAnnotation.swift
//  OrthodoxCalendar-HD
//
//   Ernest L.S. on 22.01.16.
//  Copyright © 2016 ErLS & Y. All rights reserved.
//

import Foundation
import MapKit

public class DetailsAnnotation: NSObject, MKAnnotation {
    
    public var tag: Int// = 0
    
    public var coordinate: CLLocationCoordinate2D
    
    override init() {
        self.tag = 0
        self.coordinate = CLLocationCoordinate2D()
    }
    
    
}
