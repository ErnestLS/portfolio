//
//  LocationExtented.swift
//  OrthodoxCalendar-HD
//
//   Ernest L.S. on 20.04.16.
//  Copyright © 2016 ErLS & Y. All rights reserved.
//

import CoreLocation

protocol LocationExtentedDelegate {
    func resultLocation(userLocation: CLLocation?)
}

class LocationExtented: NSObject, CLLocationManagerDelegate {
    var locationDelegate: LocationExtentedDelegate?
    var locationManager: CLLocationManager?
    
    func getLocation() {
        locationManager = CLLocationManager()
        locationManager!.delegate = self
        locationManager!.requestWhenInUseAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager!.startUpdatingLocation()
        }
        else {
            locationManager!.stopUpdatingLocation()
            //print("Нет доступа к геолокации. Измените настройки устройства.")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager!.stopUpdatingLocation()
        //print("Не удалось определить местоположение. Попробуйте позднее.")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let userLocation = locations.last {
            locationDelegate?.resultLocation(userLocation: userLocation)
        }
        else{
            locationDelegate?.resultLocation(userLocation: nil)
        }
        
        locationManager!.stopUpdatingLocation()
    }
    
    func stopUpdateLocation() {
        locationManager!.stopUpdatingLocation()
        locationManager = nil
    }
    
}

