//
//  Utils.swift
//  OrthodoxCalendar-HD
//
//   Ernest L.S. on 19.01.16.
//  Copyright © 2016 ErLS & Y. All rights reserved.
//

import UIKit
import QuartzCore

enum ButtonType {
    case
    ButtonType_Main,
    ButtonType_LeftDown,
    ButtonType_RightDown,
    ButtonType_LeftMid,
    ButtonType_RightMid,
    ButtonType_LeftUp,
    ButtonType_RightUp,
    ButtonType_MidUp
}


class UtilsAnnotation: NSObject {
    
    
    class func calculateRotateDegree(p1:CGPoint, p0: CGPoint) -> CGFloat {
        let y = abs(p1.x - p0.x)
        let x = abs(p1.y - p0.y)
        let a = CGFloat(180.0 / Double.pi )
        var rotateDegree = atan2(y, x) * a
        
        if (p0.y <= p1.y)
        {
            if (p0.x >  p1.x)
                //    {
                //        rotateDegree = rotateDegree
                //    }
                //    else
            {
                rotateDegree = -rotateDegree
            }
        }
        else
        {
            if (p0.x <= p1.x)
            {
                rotateDegree = 180.0 - rotateDegree
            }
            else
            {
                rotateDegree =  rotateDegree - 180.0
            }
        }
        return rotateDegree * CGFloat(Double.pi / 180.0 )
    }
    
    
    class func getAngleByPoint(nowPoint: CGPoint, center: CGPoint) -> CGFloat {
        
        let angle: CGFloat = UtilsAnnotation.calculateRotateDegree(p1: nowPoint, p0:center)
        return angle
        
    }
    
    class func dealViewRoundCorners(aView: UIView, aRadius: CGFloat, aBorderWidth: CGFloat, backImage: UIImage? = nil) {
        aView.layer.masksToBounds = true
        aView.layer.cornerRadius = aRadius
        if (aBorderWidth > 0) {
            aView.layer.borderColor = UIColor.blue.cgColor
            aView.layer.borderWidth = aBorderWidth
            
            if (backImage != nil) {
                let imageViewBg: UIImageView = UIImageView(frame: aView.frame)
                imageViewBg.image = backImage
                imageViewBg.layer.cornerRadius = aRadius
                imageViewBg.clipsToBounds = true
                imageViewBg.center = aView.convert(aView.center, from:aView.superview)
                aView.addSubview(imageViewBg)
            }
            
            aView.backgroundColor = UIColor.white
            
        }
        
    }
    
    class func dealScaleView(aView: UIView, aScale:CGFloat) {
        let currentTransform: CGAffineTransform  = aView.transform
        let newTransform: CGAffineTransform  = currentTransform.scaledBy(x: aScale, y: aScale)
        aView.transform = newTransform
    }
        
}
