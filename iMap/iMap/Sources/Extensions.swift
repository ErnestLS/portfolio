//
//  Extensions.swift
//  Cluster
//
//  Created by ErLS & Y.
//

import MapKit

extension MKMapRect {
    init(minX: Double, minY: Double, maxX: Double, maxY: Double) {
        self.init(x: minX, y: minY, width: abs(maxX - minX), height: abs(maxY - minY))
    }
    init(x: Double, y: Double, width: Double, height: Double) {
        self.init(origin: MKMapPoint(x: x, y: y), size: MKMapSize(width: width, height: height))
    }
//    var minX: Double { return self.minX }
//    var minY: Double { return self.minY }
//    var midX: Double { return self.midX }
//    var midY: Double { return self.midY }
//    var maxX: Double { return self.maxX }
//    var maxY: Double { return self.maxY }
//    func intersects(_ rect: MKMapRect) -> Bool {
//        return self.intersects(rect)
//    }
    func contains(_ coordinate: CLLocationCoordinate2D) -> Bool {
        return self.contains(MKMapPoint(coordinate))
    }
}
