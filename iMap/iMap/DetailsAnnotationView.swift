//
//  DetailsAnnotationView.swift
//  OrthodoxCalendar-HD
//
//   Ernest L.S. on 22.01.16.
//  Copyright © 2016 ErLS & Y. All rights reserved.
//

import MapKit
//import QuartzCore

class DetailsAnnotationView: MKAnnotationView {
    let Arror_height = 15
    
    var  cell:DetailsView!
    
//    override init(frame: CGRect) {
//        let frame = CGRectNull//Make(0, 0, 260, 260)
//        super.init(frame: frame)
//    }
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        self.canShowCallout = false
        self.frame = CGRect(x:0, y:0, width:260, height:260)
        self.cell  = DetailsView.getInstanceWithNib()
        self.addSubview(self.cell)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setCellUI(vo: MapItemInfoVO)
    {
        self.cell.tag = self.tag
        self.cell.setUI(vo: vo)
    }

}
