//
//  ItemView.swift
//  OrthodoxCalendar-HD
//
//   Ernest L.S. on 22.01.16.
//  Copyright © 2016 ErLS & Y. All rights reserved.
//

import UIKit
import QuartzCore

enum ItemButtonType {
    case
    ItemButtonType_Main,
    ItemButtonType_Left,
    ItemButtonType_right
}

public class ItemView : UIView {
    let aScale_Main = 3.0 / 5.0
    let aScale_Other = 4.0 / 5.0
    
    var button: UIButton?
    var labelBottom = UILabel()
    var labelTitle_Main = UILabel()
    
    var angleStart: CGFloat = 0.0
    var angleEnd: CGFloat = 0.0
    var lineStartP: CGPoint = CGPoint.zero
    var lineEndP: CGPoint = CGPoint.zero
    var startCenter: CGPoint = CGPoint.zero
    var endCenter: CGPoint = CGPoint.zero
    var farCenter: CGPoint = CGPoint.zero
    
    init(frame: CGRect, frameBtn:CGRect, lineStart:CGPoint, lineEnd:CGPoint)
    {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clear
        self.lineStartP = lineStart
        self.lineEndP = lineEnd
        
        self.button = UIButton(type: .custom)
        
        self.button!.frame = frameBtn
        self.button!.center = self.center
        self.button!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.button!.addTarget(self, action: #selector(ItemView.clickButton), for:.touchUpInside)
        
        self.addSubview(self.button!)
        
        if (frame.height == 260) {
            
            self.labelBottom.isHidden = true
            self.labelTitle_Main = UILabel(frame: CGRect(x:0, y:(self.button!.bounds.height/2.0 + 10.0), width:(self.button!.bounds.width - 40.0), height:20.0))
            
            self.labelTitle_Main.center  = CGPoint(x:self.button!.bounds.width/2.0, y:self.button!.bounds.height/2.0)//center1;
            self.labelTitle_Main.textColor = UIColor.black //UtilsAnnotation.getColorByTag(self.tag)
            self.labelTitle_Main.textAlignment = .center
            self.labelTitle_Main.font = UIFont.boldSystemFont(ofSize: 15.0)
            self.labelTitle_Main.backgroundColor = UIColor.clear
            
            self.button!.addSubview(self.labelTitle_Main)
            
        }
        else
        {
            
            self.labelBottom = UILabel()
            let labelFrame: CGRect  = CGRect(x:0, y:0, width:(frameBtn.width - 20), height:(frameBtn.height - 20))
            self.labelBottom.frame = labelFrame
            self.labelBottom.center  = CGPoint(x:self.button!.bounds.width/2, y:self.button!.bounds.height/2)
            self.labelBottom.font = UIFont.systemFont(ofSize: 12.0)
            self.labelBottom.adjustsFontSizeToFitWidth = true
            self.labelBottom.baselineAdjustment = .alignCenters
            self.labelBottom.textAlignment = .center
            self.labelBottom.lineBreakMode = .byClipping
            self.labelBottom.numberOfLines = 0
            self.labelBottom.minimumScaleFactor = 0.0
            self.labelBottom.textColor = UIColor.black//   UtilsAnnotation.getColorByTag(self.tag)
            self.labelBottom.backgroundColor = UIColor.clear
            
            self.button!.addSubview(self.labelBottom)
        }
    }
    
    
    public override func draw(_ rect: CGRect)
    {
        if (self.lineEndP.x == self.lineStartP.x && self.lineStartP.y == self.lineEndP.y) {
            return
        }
        self.drawInContext(context: UIGraphicsGetCurrentContext()!)
        
    }
    
    func drawInContext(context: CGContext)
    {
        context.setLineWidth(1.0)
        context.move(to: CGPoint(x:self.lineStartP.x, y:self.lineStartP.y))
        context.addLine(to: CGPoint(x:self.lineEndP.x, y:self.lineEndP.y))
        //    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);//设置线的颜色
        context.strokePath()
    }
    
    
    // MARK: - clickItemButton
    @objc func clickButton(sender:UIButton) {
        //print("Кликнули на описании (на изображении)!")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "clickAnnotationOnImageButton"), object: nil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}
