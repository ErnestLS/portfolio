
import UIKit
import MapKit

class ChurchesViewController: UIViewController {
    var locationExtented: LocationExtented!
    var currentUserLocation: CLLocation!
    
    let clasterManager = ClusterManager()
    
    @IBOutlet weak var localMapView: MKMapView!
    
    //Это "булавки"
    var currentChurchAnnotation = ChurchAnnotation()
    //Это подробности всплывающие окружности
    var currentDetailsAnnotationView = DetailsAnnotationView()
    var isParseded: Bool = false
    var isDetailView: Bool = false
    var selectedChurch: MKAnnotationView?

    var dataArray: NSMutableArray!
    
    
    // ========================================================================
    // MARK:- Live Circle
    // ========================================================================
    
    func initDataAnnotation(imageChurch: UIImage?, typeChurch: String, nameChurch: String, distance: String)
    {
        if dataArray == nil {
            dataArray = NSMutableArray()
        }
        else {
            dataArray.removeAllObjects()
        }
        //child item
        let childAry = NSMutableArray()
        // Готовим только 3 окружности (малые)
        for i in 0..<4 {
            let child: MapItemInfoVO = MapItemInfoVO()
            child.strId = "\(i)"
            switch i {
            case 0:
                child.strTitle = typeChurch
            case 1:
                child.strTitle = nameChurch
            case 2:
                child.strTitle = distance
            default: break;
            }
            childAry.add(child)
        }
        //item
        // Только одна Главная окружность!
        for j in (0..<4) {
            let item: MapItemInfoVO = MapItemInfoVO()
            //        item.strId = [NSString stringWithFormat:@"%d",j];
            //        item.strTitle = [NSString stringWithFormat:@"Item%d",j];
            if imageChurch == nil {
                item.image = UIImage(named: "DefaultChurchImage") // Здесь оставить пустым а присваивать только когда показываем?
            }
            else {
                item.image = imageChurch
            }
            
            let ary:NSMutableArray = NSMutableArray()
            
            let childCount = j
            for n in (0..<childCount) {
                if (n < childAry.count) {
                    ary.add(childAry.object(at: n))
                }
            }
            item.aryChild = ary
            dataArray.add(item)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChurchesViewController.clickOnImage), name: NSNotification.Name(rawValue: "clickAnnotationOnImageButton"), object: nil)
        localMapView.centerCoordinate = CLLocationCoordinate2DMake(0, 0)
        localMapView.showsUserLocation = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.showLocation()
    }
    
    override func viewWillDisappear(_ animated:Bool){
        super.viewWillDisappear(animated)
        applyMapViewMemoryFix()
    }
    
    func applyMapViewMemoryFix(){
        localMapView.showsUserLocation = false
        localMapView.delegate = nil
        localMapView.removeAnnotations(localMapView.annotations)
        localMapView.removeFromSuperview()
        localMapView = nil
    }
    
    // ========================================================================
    // MARK:- @IBAction func
    // ========================================================================
       
    @IBAction func showAbout() {
        let customAlertView: FCAlertView = {
            let alert = FCAlertView(type: .info)
            alert.dismissOnOutsideTouch = true
            return alert
        }()
        customAlertView.delegate = nil
        customAlertView.showAlert(inView: self, withTitle: "О программе", withSubtitle: "Храмы и монастыри. Для построения маршрута коснитесь изображения Храма.", withCustomImage: nil, withDoneButtonTitle: "Ок", andButtons: nil)
    }
    
    @IBAction func showCurrentLocation(){
        self.showLocation()
    }
    
    @IBAction func changeMapType(){
        if  localMapView.mapType == .standard {
            localMapView.mapType = .hybrid
        } else {
            localMapView.mapType = .standard
        }
    }
    
    // ========================================================================
    // MARK:- Start Map
    // ========================================================================
    
    var pathForChurches:String = {
        // getting path to GameData.plist
        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as NSArray
        let documentsDirectory = paths[0] as! NSString
        let path = documentsDirectory.appendingPathComponent("Churches.plist")
        
        //        var fileToExclude = NSURL.fileURL(withPath: path)
        //        do {
        //            try fileToExclude.setResourceValue(true as AnyObject?, forKey: URLResourceKey.isExcludedFromBackupKey)
        //        } catch _{
        //            //  print("Failed")
        //        }
        
        let fileManager = FileManager.default
        //check if file exists
        if(!fileManager.fileExists(atPath: path)) {
            // If it doesn't, copy it from the default file in the Bundle
            if let bundlePath = Bundle.main.path(forResource: "Churches", ofType: "plist") {
                let resultDictionary = NSMutableDictionary(contentsOfFile: bundlePath)
                do {
                    // Копируем файл Базы данных в пупку Cache
                    try fileManager.copyItem(atPath: bundlePath, toPath: path)
                } catch {}
                
            } else {
                //  EVLog("AppSettings.plist not found. Please, make sure it is part of the bundle.")
            }
        } else {
            //   EVLog("AppSettings.plist already exits at path.")
        }
        
        return path
    }()
    
    fileprivate func startJSON(){
        let detalArray = NSArray(contentsOfFile: pathForChurches)
        var array: [ChurchAnnotation] = []
        
 //       DispatchQueue.main.async(execute: { () -> Void in
            for detaiList in detalArray! {
                let dic = detaiList as! NSDictionary
                if let
                    churchId = (dic.object(forKey: "properties") as! NSDictionary).value(forKey: "type") as? String,
                    let coordunate = (dic.object(forKey: "geometry") as! NSDictionary).value(forKey: "coordinates") as? NSArray,
                    let note = (dic.object(forKey: "properties") as! NSDictionary).value(forKey: "hintContent") as? String,
                    let imageID = dic.value(forKey: "id") as? NSNumber
                {
                    let imageStr: String = "http://map.patriarhia.ru/wp-content/blogs.dir/\(imageID.stringValue)/files/i-150x150.png"
                    let newChurchAnnotation = ChurchAnnotation(churchId: churchId, latitude: coordunate[0] as! Double, longitude: coordunate[1] as! Double, note: note, imageStr: imageStr)
                    array.append(newChurchAnnotation)
                }
            }
            
            self.clasterManager.add(array)
            self.clasterManager.reload(self.localMapView)
            self.isParseded = true
 //       })
    }
    
    @objc func clickOnImage() {
        showMenuRoute(sender: UIButton())
    }
    
    // ========================================================================
    // MARK:- Расчет и прорисовка пути к Храму
    // ========================================================================
    
    var currentTransportType: MKDirectionsTransportType = []
    private func drawRouteToChurch(transportType: MKDirectionsTransportType) {
        
        let churchCoordinate: CLLocationCoordinate2D =  currentChurchAnnotation.coordinate
        let place = MKPlacemark(coordinate: churchCoordinate, addressDictionary: nil)
        let destination: MKMapItem = MKMapItem(placemark: place)
        
        let request = MKDirections.Request()
        request.source = MKMapItem.forCurrentLocation()
        request.destination = destination
        request.requestsAlternateRoutes = false
        
        request.transportType = transportType
        
        let directions = MKDirections(request: request)
        
        directions.calculate { (response, error) -> Void in
            if error != nil {
                //print("Ошибка при определении маршрута движения")
            } else {
                self.showRoute(response: response!)
            }
            
        }
    }
    
    
    func showRoute(response: MKDirections.Response){
        var routeDistance: CLLocationDistance = 1000 //Просто инициализация
        for route in response.routes {
            routeDistance = route.distance
            currentTransportType = route.transportType
            localMapView.addOverlay(route.polyline, level: MKOverlayLevel.aboveRoads)
            //Инструкция движения !!! Возможно использовать в будущем :)
            //            for step in route.steps {
            //                print("Инструкция движения: \(step.instructions)")
            //            }
        }
        
        // Изменить параметры региона чтоб помещались Старт и Финиш
        let startCoordinate: CLLocationCoordinate2D = currentUserLocation!.coordinate
        let finishCoordinate: CLLocationCoordinate2D = currentChurchAnnotation.coordinate
        let latitudeCenter = (startCoordinate.latitude + finishCoordinate.latitude) / 2
        let longitudeCenter = (startCoordinate.longitude + finishCoordinate.longitude) / 2
        let center:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitudeCenter, longitude: longitudeCenter)
        let region:MKCoordinateRegion = MKCoordinateRegion(center: center, latitudinalMeters: routeDistance, longitudinalMeters: routeDistance)
        
        localMapView.setRegion(region, animated: true)
        //print("Расстояние маршрута: \(routeDistance)")
    }
    
    // ========================================================================
    // MARK:- Вызов меню для прорисовки маршрута
    // ========================================================================
    
    let itemTitleCar  = "На автомобиле"
    let itemTitleWalk = "Пешком"
    let itemTitleСompare = "Сравнить"
    private func showMenuRoute(sender: UIButton)  {
        let titleMenu: KxMenuItem =  KxMenuItem("Показать маршрут", image:nil, target:nil, action:nil)
        let itemCarMenu: KxMenuItem =  KxMenuItem(itemTitleCar, image:UIImage(named: "IconCar"), target:self, action: #selector(ChurchesViewController.pushMenuItem))
        let itemWalkMenu: KxMenuItem =  KxMenuItem(itemTitleWalk, image:UIImage(named: "IconWalk"), target:self, action: #selector(ChurchesViewController.pushMenuItem))
        let itemСompareMenu: KxMenuItem =  KxMenuItem(itemTitleСompare, image:UIImage(named: "IconCompare"), target:self, action: #selector(ChurchesViewController.pushMenuItem))
        let itemCancel: KxMenuItem =  KxMenuItem("Отменить", image:nil, target:self, action: #selector(ChurchesViewController.pushMenuItem))
        let menuItems: NSArray = [titleMenu, itemCarMenu, itemWalkMenu, itemСompareMenu, itemCancel]
        let first:KxMenuItem = menuItems[0] as! KxMenuItem
        first.foreColor = UIColor(red:47/255.0, green:112/255.0, blue:225/255.0, alpha:1.0)
        first.alignment = .center
        
        KxMenu.show(in: self.view, from:sender.frame, menuItems: menuItems as [AnyObject])
    }
    
    @objc func pushMenuItem(sender: AnyObject) {
        //print("Коснулись пункта меню - \(sender)")
        let overlays = localMapView.overlays
        localMapView.removeOverlays(overlays)
        
        let item = sender as! KxMenuItem
        switch item.title! {
        case itemTitleCar:  drawRouteToChurch(transportType: .automobile)
        case itemTitleWalk:  drawRouteToChurch(transportType: .walking)
        case itemTitleСompare:
            drawRouteToChurch(transportType: .automobile)
            drawRouteToChurch(transportType: .walking)
        default: break
        }
    }
    
}

// ========================================================================
// MARK:- Расширение класса для LocationExtentedDelegate
// ========================================================================

extension ChurchesViewController: LocationExtentedDelegate {
    
    func showLocation(){
        if locationExtented == nil {
            locationExtented = LocationExtented()
            locationExtented.locationDelegate = self
        }
        locationExtented.getLocation()
    }
    
    func resultLocation(userLocation: CLLocation?) {
        currentUserLocation = userLocation
        if userLocation != nil {
            let regionUser = MKCoordinateRegion(center: userLocation!.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
            localMapView.setRegion(localMapView.regionThatFits(regionUser), animated:true)
        }
        else {
            //print("Не удалось определить местоположение. Попробуйте позднее.")
        }
    }
    
 }

// ========================================================================
// MARK: - MKMapViewDelegate
// ========================================================================

extension ChurchesViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        if isDetailView {
            let dV:DetailsAnnotationView = (mapView.view(for: currentDetailsAnnotationView.annotation!) as? DetailsAnnotationView)!
            dV.superview!.bringSubviewToFront(dV)
        }
    }

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        guard isParseded else {return}
        
        if isDetailView {
            isDetailView = false
            localMapView.deselectAnnotation(selectedChurch?.annotation!, animated: false)
            self.clasterManager.reload(mapView)
        }
        else {
            self.clasterManager.reload(mapView)
        }
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let color = UIColor(red: 128/255, green: 0/255, blue: 128/255, alpha: 1)
        
        switch annotation {
        case is ClusterAnnotation:
            let identifier = "Cluster"
            var claster = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if claster == nil {
                claster = ClusterAnnotationView(annotation: annotation, reuseIdentifier: identifier, type: .color(color: color, radius: 25))
            }
            else {
                claster?.annotation = annotation
            }
            
            return claster

        case is ChurchAnnotation:
            let identifier = "Church"
            var church = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if church == nil {
                church = ChurchAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                church!.centerOffset = CGPoint(x:0, y:-church!.frame.size.height/2)
            }
            
            church!.annotation = annotation
            let churchAnnotation = annotation as! ChurchAnnotation
            
            switch churchAnnotation.churchId {
            case "7": // Епархии
                church!.image = UIImage(named: "PinEparchy")
            case "19": // Кафедральные соборы
                church!.image = UIImage(named: "PinCathedral")
            case "4", "22", "27", "28", "43": // Монастыри
                church!.image = UIImage(named: "PinMonastery")
            case "3", "11", "12", "14", "15", "30", "40": // Храмы
                church!.image = UIImage(named: "PinChurch")
            default: church!.image = UIImage(named: "PinChurch")
            }
            
            return church
            
        case is DetailsAnnotation:
            let identifier = "Details"
            let details:DetailsAnnotation = annotation as! DetailsAnnotation
            let num = details.tag
            currentDetailsAnnotationView = DetailsAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            currentDetailsAnnotationView.tag = 0//num
            let vo:MapItemInfoVO = dataArray.object(at: num) as! MapItemInfoVO
            currentDetailsAnnotationView.setCellUI(vo: vo)
            
            let selectCenter:CGPoint = mapView.convert(annotation.coordinate, toPointTo: self.view)
            let center:CGPoint  = self.view.center//[mapView convertCoordinate:_mapView.centerCoordinate toPointToView:self.view];
            let angle: CGFloat  = UtilsAnnotation.getAngleByPoint(nowPoint: selectCenter, center:center)
            let newCenter:CGPoint  = CGPoint(x:(currentDetailsAnnotationView.cell.center.x - (currentDetailsAnnotationView.cell.bounds.size.width/2 * sin(angle))), y:(currentDetailsAnnotationView.cell.center.y - (currentDetailsAnnotationView.cell.bounds.size.width/2 * cos(angle))))
            
            currentDetailsAnnotationView.cell.toAppearItemsView(aPoint: newCenter, aAngle:angle)
            
            return currentDetailsAnnotationView
            
        default:
            return nil
        }
    }
    
    private func newDetailView(_ mapView: MKMapView, didSelect view: MKAnnotationView){
        
        guard let currentAnnotation = view.annotation as? ChurchAnnotation else {
            //Ошибка при отображении Аннотации
            return
        }
        
        currentChurchAnnotation = currentAnnotation
        
        let locationPin: CLLocation = CLLocation(latitude: currentChurchAnnotation.coordinate.latitude, longitude: currentChurchAnnotation.coordinate.longitude)
        var distanceToChurch: String = "От Вас:\n??? м"
        var imageChurch: UIImage? = nil
        if currentUserLocation != nil {
            let dist = Int(currentUserLocation!.distance(from: locationPin))
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.groupingSeparator = "'"
            let num = NSNumber(value: dist)
            let distance = formatter.string(from: num)
            distanceToChurch = "От Вас:\n\(distance!) м"
        }
        
        let imageURL = NSURL(string: currentChurchAnnotation.imageStr!)
        if let dataImage = NSData(contentsOf: imageURL! as URL) {
            imageChurch = UIImage(data: dataImage as Data)
        }
        
        let nameChurch = currentChurchAnnotation.title ?? ""
        let typeChurch: String
        switch currentChurchAnnotation.churchId {
        case "7": // Епархии
            typeChurch = "Епархия"
        case "19": // Кафедральные соборы
            typeChurch = "Кафедральный собор"
        case "4", "22", "27", "28", "43": // Монастыри
            typeChurch = "Монастырь"
        case "3", "11", "12", "14", "15", "30", "40": // Храмы
            typeChurch = "Храм"
        default: typeChurch = "Церковь"
        }
        
        initDataAnnotation(imageChurch: imageChurch, typeChurch: typeChurch, nameChurch: nameChurch, distance: distanceToChurch)
        
        let detailsAnno = DetailsAnnotation()
        detailsAnno.tag = 3// segmentController.selectedSegmentIndex;
        detailsAnno.coordinate = (view.annotation?.coordinate)!
        
        mapView.addAnnotation(detailsAnno)
        isDetailView = true
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView){
        guard view is ChurchAnnotationView else { return }
        
        selectedChurch = view

        if isDetailView {
            currentDetailsAnnotationView.cell.disappearItems {
                self.newDetailView(mapView, didSelect: view)
            }
        }
        else {
            newDetailView(mapView, didSelect: view)
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        currentDetailsAnnotationView.cell.disappearItems {
            self.isDetailView = false
        }
    }

    // Для прорисовки Маршрута
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = currentTransportType == .automobile ? UIColor.blue : UIColor.green
        renderer.lineWidth = 5.0
        return renderer
    }
    
    func mapViewDidFinishRenderingMap(_ mapView: MKMapView, fullyRendered: Bool) {
        guard !isParseded else {return}
        print("fullyRendered - \(fullyRendered)")

        startJSON()
    }
    
}
