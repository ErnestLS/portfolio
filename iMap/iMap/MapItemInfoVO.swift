//
//  MapItemInfoVO.swift
//  OrthodoxCalendar-HD
//
//   Ernest L.S. on 19.01.16.
//  Copyright © 2016 ErLS & Y. All rights reserved.
//

import UIKit

class MapItemInfoVO: NSObject {
    var strId: String?
    var strTitle: String?
    var strDetails: String?
    var strLat: String?
    var strLon: String?
    var aryChild: NSMutableArray?
    
    var image: UIImage?

}
