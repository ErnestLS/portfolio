//
//  DetailsView.swift
//  OrthodoxCalendar-HD
//
//   Ernest L.S. on 22.01.16.
//  Copyright © 2016 ErLS & Y. All rights reserved.
//

import UIKit
import QuartzCore


let ANGLE_TO_PI = Double.pi / 180
let SPEEDTIME_TO_PI = Double.pi / 180

let SpaceX = 5.0
let SpaceY = 5.0

let scaleNum =   0.01
let animationTime = 0.4

let fSmallCircleD = 80//Малый радиус
let fSmallCircleD_Shadow = 80
let fBigCircleD = 140    //Большой радиус
let fBigCircleD_Shadow = 140

let fSmallRectD_UD = 83      //Длина линии
let  fSmallRectD_Mid = Float(fSmallRectD_UD) / sinf(Float(Double.pi / 4))

let fBigRectD1 = 260

let spaceFar = 2.5


public class DetailsView: UIView {
    typealias disappearBlock = () -> Void
    
//    var selectItem: ItemView!
    
    var startAngle: CGFloat = 0.0
    
    var itemsNum: Int = 0
    var itemsAry: NSMutableArray = []
    
//    var flickerTimer: NSTimer? = nil
    
    func pStartCenter() -> CGPoint {
        return CGPoint(x:self.center.x, y:(self.bounds.height - CGFloat(fBigRectD1 / 2)))
    }
    
    func fSpaceCenterY() -> CGFloat {
        return self.bounds.height - CGFloat(fBigRectD1/2) - self.center.y
    }
    
    func spaceH() -> CGFloat {
        let temp1 = (self.bounds.height - standRect.height)/2.0
        let temp2 = self.fSpaceCenterY()
        return temp1 + temp2
    }
    
    func spaceW() -> CGFloat    {
        return (self.bounds.width -  standRect.width)/2.0
    }
    
    let standRect = CGRect(x:0, y:0, width:260, height:260)  //Площадка в коророй размещаются все
    
    class func getInstanceWithNib() -> DetailsView!
    {
        let detailView = Bundle(for: self).loadNibNamed("DetailsView", owner:nil, options:nil)!.first as! DetailsView
        return detailView
    }
    
    
    func setUI(vo: MapItemInfoVO)
    {
        let childAray: NSMutableArray = vo.aryChild!
        itemsNum = childAray.count + 1
        
        if self.itemsAry.count > 0 {
            self.itemsAry.removeAllObjects()
        }
        
        
        /********** Круг Изображение Храма **************/
        let frame0:CGRect  = CGRect(x:0, y:0, width:CGFloat(fBigRectD1), height:CGFloat(fBigRectD1))
        let ps0: CGPoint  = CGPoint(x:frame0.width/2, y:CGFloat(fBigRectD1 - (fBigRectD1 - fBigCircleD)/2) - CGFloat((fBigCircleD_Shadow - fBigCircleD)))
        let pe0:CGPoint  = CGPoint(x:frame0.width/2, y:frame0.height)
        let frameBtn = CGRect(x:0, y:0, width:CGFloat(fBigCircleD_Shadow), height:CGFloat(fBigCircleD_Shadow))
        //  let aBlock = block:^(id btn) { [self clickButton:(ItemView *)btn] }
        let buttonItem0: ItemView  = ItemView(frame: frame0, frameBtn: frameBtn, lineStart: ps0, lineEnd: pe0)//, aBlock: aBlock)
        
        UtilsAnnotation.dealViewRoundCorners(aView: buttonItem0.button!, aRadius:buttonItem0.button!.bounds.height/2, aBorderWidth:2.0, backImage:vo.image)
        
//        buttonItem0.superview!.backgroundColor = UIColor.yellowColor()
//        buttonItem0.superview!.superview!.backgroundColor = UIColor.grayColor()
        
//        buttonItem0.labelTitle_Main.text = vo.strTitle
//        buttonItem0.labelTitle_Main.textColor = UIColor.blackColor()//       UtilsAnnotation.getColorByTag(self.tag)
        
        buttonItem0.startCenter = CGPoint(x:self.bounds.width/2, y:self.bounds.height)
        buttonItem0.endCenter = pStartCenter()
        buttonItem0.farCenter = CGPoint(x:buttonItem0.endCenter.x, y:(buttonItem0.endCenter.y - CGFloat(spaceFar * 2)))
        buttonItem0.center = buttonItem0.startCenter
        
        self.addSubview(buttonItem0)
        buttonItem0.tag = 0//ButtonType_Main
        
        self.itemsAry.add(buttonItem0)
        
        
        /**************************************** Левый нижний круг ******************************************************/
         //     let aBlock = block:^(id btn) { [self clickButton:(ItemView *)btn] }
        let mpi4Floaf = Float(Double.pi / 4)
        
        if (itemsNum > 1) {
            let frameUD:CGRect  = CGRect(x:0, y:0, width:CGFloat(fSmallRectD_UD), height:CGFloat(fSmallRectD_UD))
            let ps1:CGPoint  = CGPoint(x:(frameUD.width/2 + CGFloat(cosf(mpi4Floaf) * Float(fSmallCircleD/2))), y:(frameUD.height/2 - CGFloat(sinf(Float(Double.pi / 4)) * Float(fSmallCircleD/2))))
            let pe1:CGPoint  = CGPoint(x:frameUD.width, y:0)
            let btnFrame = CGRect(x:0, y:0, width:CGFloat(fSmallCircleD_Shadow), height:CGFloat(fSmallCircleD_Shadow))
            let buttonItem1:ItemView = ItemView(frame: frameUD, frameBtn:btnFrame, lineStart:ps1, lineEnd:pe1)//, aBlock:aBlock)
            
            buttonItem1.startCenter = pStartCenter()
            
            buttonItem1.endCenter = CGPoint(x:(buttonItem1.button!.bounds.width/2 + spaceW()), y:(self.bounds.height - buttonItem1.button!.bounds.height/2 - spaceH()))
            buttonItem1.farCenter = CGPoint(x:(buttonItem1.endCenter.x - CGFloat(cosf(mpi4Floaf) * Float(spaceFar))), y:(buttonItem1.endCenter.y + CGFloat(sinf(mpi4Floaf) * Float(spaceFar))))
            buttonItem1.center = buttonItem1.startCenter
            self.insertSubview(buttonItem1, belowSubview:buttonItem0)
            buttonItem1.isHidden = true
            buttonItem1.tag = 1//ButtonType_LeftDown
            self.itemsAry.add(buttonItem1)
            
            
            
            /**************************************** правый нижний круг ******************************************************/
            if (itemsNum > 2) {
                let ps2:CGPoint  = CGPoint(x:(frameUD.width/2 - CGFloat(cosf(mpi4Floaf) * Float(fSmallCircleD/2))), y:(frameUD.height/2 - CGFloat(sinf(mpi4Floaf) * Float(fSmallCircleD/2))))
                let pe2:CGPoint  = CGPoint.zero
                let buttonFrame = CGRect(x:0, y:0, width:CGFloat(fSmallCircleD_Shadow), height:CGFloat(fSmallCircleD_Shadow))
                let buttonItem2:ItemView = ItemView(frame: frameUD, frameBtn:buttonFrame, lineStart:ps2, lineEnd:pe2)//, aBlock:aBlock)
                buttonItem2.startCenter = pStartCenter()
                buttonItem2.endCenter = CGPoint(x:(self.bounds.width - spaceW() - buttonItem2.button!.bounds.width/2), y:(self.bounds.height - buttonItem2.button!.bounds.height/2 - spaceH()))
                buttonItem2.farCenter = CGPoint(x:(buttonItem2.endCenter.x + CGFloat(cosf(mpi4Floaf) * Float(spaceFar))), y:(buttonItem2.endCenter.y + CGFloat(sinf(mpi4Floaf) * Float(spaceFar))))
                buttonItem2.center = buttonItem2.startCenter
                self.insertSubview(buttonItem2, belowSubview:buttonItem0)
                buttonItem2.isHidden = true
                buttonItem2.tag = 2//ButtonType_RightDown
                self.itemsAry.add(buttonItem2)
                
                
                /**************************************** левый средний круг ******************************************************/
                if (itemsNum > 3) {
                    
                    let frameMid:CGRect  = CGRect(x:0, y:0, width:CGFloat(fSmallRectD_Mid), height:CGFloat(fSmallRectD_Mid))
                    let ps3:CGPoint  = CGPoint(x:(frameMid.width + CGFloat(fSmallCircleD))/2, y:frameMid.height/2)
                    let pe3:CGPoint  = CGPoint(x:frameMid.width, y:frameMid.height/2)
                    let buttonFrame = CGRect(x:0, y:0, width:CGFloat(fSmallCircleD_Shadow), height:CGFloat(fSmallCircleD_Shadow))
                    let buttonItem3:ItemView = ItemView(frame: frameMid, frameBtn:buttonFrame, lineStart:ps3, lineEnd:pe3)//, aBlock:aBlock)
                    buttonItem3.startCenter = pStartCenter()
                    buttonItem3.endCenter = CGPoint(x:(buttonItem3.startCenter.x - CGFloat(Float(fBigCircleD/2) + fSmallRectD_Mid/2)), y:buttonItem3.startCenter.y)
                    buttonItem3.farCenter = CGPoint(x:(buttonItem3.endCenter.x - CGFloat(spaceFar)), y:buttonItem3.endCenter.y)
                    
                    buttonItem3.center = buttonItem3.startCenter
                    
                    self.insertSubview(buttonItem3, belowSubview:buttonItem0)
                    buttonItem3.isHidden = true
                    buttonItem3.tag = 3//ButtonType_LeftMid
                    self.itemsAry.add(buttonItem3)
                    
                    
                    //        /**************************************** правый средний круг ******************************************************/
                    //        if (_itemsNum > 4) {
                    //            CGPoint ps4 = CGPointMake((CGRectGetWidth(frameMid)-fSmallCircleD)/2., CGRectGetHeight(frameMid)/2.) ;
                    //            CGPoint pe4 = CGPointMake(0., CGRectGetHeight(frameMid)/2.);
                    //            ItemView * buttonItem4= [[ItemView alloc]initWithFrame:frameMid
                    //                buttonFrame:CGRectMake(0, 0, fSmallCircleD_Shadow, fSmallCircleD_Shadow)
                    //                lineStart:ps4
                    //                lineEnd:pe4
                    //                block:^(id btn) {
                    //                [self clickButton:(ItemView *)btn];
                    //                }];
                    //            buttonItem4.startCenter = pStartCenter;
                    //            buttonItem4.endCenter =CGPointMake(buttonItem4.startCenter.x + (fBigCircleD/2. + fSmallRectD_Mid/2.), buttonItem4.startCenter.y);
                    //            buttonItem4.farCenter = CGPointMake(buttonItem4.endCenter.x + spaceFar, buttonItem4.endCenter.y) ;
                    //            buttonItem4.center = buttonItem4.startCenter;
                    //            [self insertSubview:buttonItem4 belowSubview:buttonItem0];
                    //            buttonItem4.hidden = YES;
                    //            buttonItem4.tag = ButtonType_RightMid;
                    //
                    //            [self.itemsAry addObject:buttonItem4];
                    //
                    //            /**************************************** левый верхний круг ******************************************************/
                    //            if (_itemsNum > 5) {
                    //
                    //                //leftup button
                    //                CGPoint ps5 = CGPointMake(CGRectGetWidth(frameUD)/2.+cosf(M_PI_4)*fSmallCircleD/2., CGRectGetHeight(frameUD)/2.+sinf(M_PI_4)*fSmallCircleD/2.) ;
                    //                CGPoint pe5 = CGPointMake(CGRectGetWidth(frameUD), CGRectGetHeight(frameUD));
                    //                ItemView * buttonItem5= [[ItemView alloc]initWithFrame:frameUD
                    //                    buttonFrame:CGRectMake(0, 0, fSmallCircleD_Shadow, fSmallCircleD_Shadow)
                    //                    lineStart:ps5
                    //                    lineEnd:pe5
                    //                    block:^(id btn) {
                    //                    [self clickButton:(ItemView *)btn];
                    //                    }];
                    //                buttonItem5.startCenter = pStartCenter;
                    //                buttonItem5.endCenter = CGPointMake((CGRectGetWidth(buttonItem5.button.bounds)/2.+spaceW), CGRectGetHeight(buttonItem5.button.bounds)/2.+spaceH);
                    //                buttonItem5.farCenter = CGPointMake(buttonItem5.endCenter.x - cosf(M_PI_4)*spaceFar, buttonItem5.endCenter.y - sinf(M_PI_4)*spaceFar);
                    //                buttonItem5.center = buttonItem5.startCenter;
                    //                [self insertSubview:buttonItem5 belowSubview:buttonItem0];
                    //                buttonItem5.hidden = YES;
                    //                buttonItem5.tag = ButtonType_LeftUp;
                    //                [self.itemsAry addObject:buttonItem5];
                    //
                    //                /**************************************** правый верхний круг ******************************************************/
                    //                if (_itemsNum > 6) {
                    //                    CGPoint ps6 = CGPointMake(CGRectGetWidth(frameUD)/2.-cosf(M_PI_4)*fSmallCircleD/2., CGRectGetHeight(frameUD)/2.+sinf(M_PI_4)*fSmallCircleD/2.) ;
                    //                    CGPoint pe6 = CGPointMake(0, CGRectGetHeight(frameUD));
                    //                    ItemView * buttonItem6= [[ItemView alloc]initWithFrame:frameUD
                    //                        buttonFrame:CGRectMake(0, 0, fSmallCircleD_Shadow, fSmallCircleD_Shadow)
                    //                        lineStart:ps6
                    //                        lineEnd:pe6
                    //                        block:^(id btn) {
                    //                        [self clickButton:(ItemView *)btn];
                    //                        }];
                    //                    buttonItem6.startCenter = pStartCenter;
                    //                    buttonItem6.endCenter = CGPointMake((CGRectGetWidth(self.bounds) -spaceW- CGRectGetWidth(buttonItem6.button.bounds)/2.),CGRectGetHeight(buttonItem6.button.bounds)/2.+spaceH);
                    //                    buttonItem6.farCenter = CGPointMake(buttonItem6.endCenter.x + cosf(M_PI_4)*spaceFar, buttonItem6.endCenter.y - sinf(M_PI_4)*spaceFar);
                    //                    buttonItem6.center = buttonItem6.startCenter;
                    //                    [self insertSubview:buttonItem6 belowSubview:buttonItem0];
                    //                    buttonItem6.hidden = YES;
                    //                    buttonItem6.tag = ButtonType_RightUp;
                    //
                    //                    [self.itemsAry addObject:buttonItem6];
                    //
                    //
                    //                    /**************************************** midUp button ******************************************************/
                    //                    if (_itemsNum > 7) {
                    //                        //midUp button
                    //                        CGPoint ps7 = CGPointMake(CGRectGetWidth(frameMid)/2., (CGRectGetHeight(frameMid) - fSmallCircleD)/2.+fSmallCircleD);
                    //                        CGPoint pe7 = CGPointMake(CGRectGetWidth(frameMid)/2., CGRectGetHeight(frameMid));
                    //                        ItemView * buttonItem7= [[ItemView alloc]initWithFrame:frameMid
                    //                            buttonFrame:CGRectMake(0, 0, fSmallCircleD_Shadow, fSmallCircleD_Shadow)
                    //                            lineStart:ps7
                    //                            lineEnd:pe7
                    //                            block:^(id btn) {
                    //                            [self clickButton:(ItemView *)btn];
                    //                            }];
                    //                        buttonItem7.startCenter = pStartCenter;
                    //                        buttonItem7.endCenter = CGPointMake(buttonItem7.startCenter.x, buttonItem7.startCenter.y - (fBigCircleD/2. + fSmallRectD_Mid/2.));
                    //                        buttonItem7.farCenter = CGPointMake(buttonItem7.endCenter.x, buttonItem7.endCenter.y - spaceFar);
                    //                        buttonItem7.center = buttonItem4.startCenter;
                    //                        [self insertSubview:buttonItem7 belowSubview:buttonItem0];
                    //                        buttonItem7.hidden = YES;
                    //                        buttonItem7.tag = ButtonType_MidUp;
                    //
                    //                        [self.itemsAry addObject:buttonItem7];
                    //
                    //                    }
                    //
                    //                }
                    //
                    //            }
                    //        }
                    //
                }
            }
            
            
        }
        
        
        for i in (0 ..< itemsNum) {
            let item:ItemView = itemsAry.object(at: i) as! ItemView
            if (i != 0) {
                item.labelBottom.text = (vo.aryChild![i-1] as! MapItemInfoVO).strTitle//   NSString(format:"Очень большое, короткие слова но, описание длинное предложение, умещается? %d",i) as String
                UtilsAnnotation.dealViewRoundCorners(aView: item.button!, aRadius:item.button!.bounds.height/2, aBorderWidth:1.0, backImage: nil)//切成圆
                
            }
            // UIColor *color = [UtilsAnnotation getColorByTag:self.tag - 100];
            item.labelTitle_Main.textColor = UIColor.black//color
            item.labelBottom.textColor = UIColor.black//color
            UtilsAnnotation.dealScaleView(aView: item, aScale:CGFloat(scaleNum))
            
        }
        
    }
    
    
    func disappearItems(aBlock: @escaping disappearBlock) {
//        if ((self.selectItem) != nil) {
//            self.selectItem = nil
//            
//            self.flickerTimer!.invalidate()
//            let imgView:UIView = self.viewWithTag(1000)!
//            imgView.removeFromSuperview()
//        }
        
        let buttonItem0:ItemView = itemsAry.object(at: 0) as! ItemView
        if (buttonItem0.center.x == buttonItem0.startCenter.x
            && buttonItem0.center.y == buttonItem0.startCenter.y) {
                return
        }
        
        self.bringSubviewToFront(buttonItem0)
        if (itemsNum == 1) {
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                buttonItem0.center = buttonItem0.startCenter;
                UtilsAnnotation.dealScaleView(aView: buttonItem0, aScale:CGFloat(scaleNum))
                }, completion: { (Bool) -> Void in
                    aBlock()
            })
            
            
        }
        else
        {
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                for i in (1..<self.itemsNum) {
                    let item:ItemView = self.itemsAry.object(at: i) as! ItemView
                    item.center = item.startCenter
                    UtilsAnnotation.dealScaleView(aView: item, aScale:CGFloat(scaleNum))
                }
                }, completion: { (Bool) -> Void in
                    for i in (1..<self.itemsNum) {
                        let item:ItemView  = self.itemsAry.object(at: i) as! ItemView
                        item.isHidden = true
                    }
                    
                    UIView.animate(withDuration: 0.2, animations: { () -> Void in
                        buttonItem0.center = buttonItem0.startCenter
                        UtilsAnnotation.dealScaleView(aView: buttonItem0, aScale:CGFloat(scaleNum))
                         }, completion: { (Bool) -> Void in
                            aBlock()
                    })
                    
                    
            })
        }
    }
    
    func setItemsTransform(transf:CGAffineTransform)
    {
        for i in (0..<itemsNum) {
            let item:ItemView  = itemsAry.object(at: i) as! ItemView
            item.button!.transform = transf
            
        }
    }
    
    
    func toAppearItemsView(aPoint:CGPoint, aAngle:CGFloat)
    {
        self.startAngle = aAngle
        
        self.superview!.transform = .identity
        let transformTemp:CGAffineTransform  = CGAffineTransform(rotationAngle: -aAngle)
        self.superview!.layer.anchorPoint = CGPoint(x:0.5, y:1.0)
        self.superview!.transform = transformTemp
        self.setItemsTransform(transf: .identity)
        
        let transformItem:CGAffineTransform  = CGAffineTransform(rotationAngle: aAngle)
        self.setItemsTransform(transf: transformItem)
        
        self.appearItems()
        
    }
    
    func appearItems()
    {
        let buttonItem0:ItemView  = itemsAry.object(at: 0) as! ItemView
        self.appearItem(item: buttonItem0, delay:0, bFont:false)
        
        for i in (1..<itemsNum) {
            let childItem:ItemView = itemsAry.object(at: i) as! ItemView
            childItem.isHidden = false
            self.appearItem(item: childItem, delay:0.2 + Double(i - 1) * 0.1, bFont:false)
        }
        
    }
    
    func appearItem(item:ItemView, delay:Double, bFont:Bool)
    {
        UIView.animate(withDuration: 0.2, delay: delay, options: .curveEaseInOut, animations: { () -> Void in
            item.center = item.farCenter
            UtilsAnnotation.dealScaleView(aView: item, aScale: CGFloat(1/scaleNum))
            }, completion: { (Bool) -> Void in
                UIView.animate(withDuration: 0.15, animations: { () -> Void in
                    item.center = item.endCenter
                    }, completion: { (Bool) -> Void in
                        if (bFont) {
                            self.bringSubviewToFront(item)
                            
                        }
                })
        })
        
    }
    
    
    func rotationViews(aAngle: CGFloat)
    {
        var time:Double  = animationTime/2
        
        var spaceNum:CGFloat  = abs(self.startAngle)+abs(aAngle)
        if (spaceNum > CGFloat(Double.pi )) {
            spaceNum = CGFloat(M_2_PI) - spaceNum
        }
        if spaceNum > CGFloat(Double.pi / 2) {
            time = animationTime
            if spaceNum > CGFloat(Double.pi / 2 + Double.pi / 4) {
                time = animationTime * 3/2
            }
        }
        
        self.startAngle = aAngle
        
        UIView.animate(withDuration: time) { () -> Void in
            let transform:CGAffineTransform  = CGAffineTransform(rotationAngle: -aAngle)
            self.superview!.layer.anchorPoint = CGPoint(x:0.5, y:1.0)
            self.superview!.transform = transform
            
            let transformItem:CGAffineTransform  = CGAffineTransform(rotationAngle: aAngle)
            self.setItemsTransform(transf: transformItem)
            
        }
        
    }
    
    
}
